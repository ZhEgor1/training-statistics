package com.example.trainingstatistics;

public class TABLE_SETS {
    public static final String TABLE_NAME = "Sets";
    public static final String KEY_SETINDEX = "setIndex";
    public static final String KEY_EXERCISESID = "exercisesId";
    public static final String KEY_COMPLEXID = "compexId";
    public static final String KEY_REPSLEFT = "repsLeft";
    public static final String KEY_REPSRIGHT = "repsRight";
    public static final String KEY_WEIGHT = "weight";
    public static final String KEY_NOTE = "note";
}