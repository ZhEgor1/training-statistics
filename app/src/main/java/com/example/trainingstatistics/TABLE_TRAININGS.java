package com.example.trainingstatistics;

public class TABLE_TRAININGS {
    public static final String TABLE_NAME = "Trainings";
    public static final String KEY_ID = "_id";
    public static final String KEY_NAME = "name";
    public static final String KEY_NOTE = "note";
    public static final String KEY_DATE = "date";
    public static final String KEY_USERID = "userId";
}