package com.example.trainingstatistics;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.FormatFlagsConversionMismatchException;
import java.util.HashMap;
import java.util.Map;

public class EditorActivity extends AppCompatActivity implements View.OnClickListener{

    Button btnEdit;
    Cursor cursor;
    DBHelper dbHelper;
    SQLiteDatabase db;
    Boolean stateOfButtonExercisesEditor;
    Boolean stateOfButtonMuscleGroupsEditor;
    ConstraintLayout clEditor;
    Map<Integer, Boolean> stateOfEditors;
    LayoutInflater layoutInflater;
    TextView tvExercisesSwitch, tvMuscleGroupsSwitch;
    View vExercisesEditor, vMuscleGroupsEditor;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editor);

        stateOfEditors = new HashMap<>();
        stateOfEditors.put(R.layout.exercises_editor_view, true);
        stateOfEditors.put(R.layout.muscle_groups_editor_view, false);


        clEditor = findViewById(R.id.clEditor);
        layoutInflater = (LayoutInflater) getBaseContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        stateOfButtonExercisesEditor = false;
        stateOfButtonMuscleGroupsEditor = false;

        tvExercisesSwitch = findViewById(R.id.tvExercisesSwitch);
        tvExercisesSwitch.setOnClickListener(this);
        tvMuscleGroupsSwitch = findViewById(R.id.tvMuscleGroupsSwitch);
        tvMuscleGroupsSwitch.setOnClickListener(this);

        vExercisesEditor = layoutInflater.inflate(R.layout.exercises_editor_view, null);
        vMuscleGroupsEditor = layoutInflater.inflate(R.layout.muscle_groups_editor_view, null);
        dbHelper = new DBHelper(this);
        db = dbHelper.getWritableDatabase();

        btnEdit = findViewById(R.id.btnEdit);
        btnEdit.setOnClickListener(this);

        initExercisesEditor();
        initMuscleGroupsEditor();
        clEditor.addView(vExercisesEditor, 0);
    }


    private void initMuscleGroupsEditor()
    {
        Spinner sMuscleGroupsList = vMuscleGroupsEditor.findViewById(R.id.sMuscleGroupsList);
        sMuscleGroupsList.setAdapter(initSList(TABLE_MUSCLEGROUPS.TABLE_NAME, "Create new"));
        sMuscleGroupsList.setOnItemSelectedListener(onItemSelectedListener());

        Spinner sMajorMuscleGroupsList = vMuscleGroupsEditor.findViewById(R.id.sMajorMuscleGroupsList);
        sMajorMuscleGroupsList.setAdapter(initSList(TABLE_MAJORMUSCLEGROUPS.TABLE_NAME, null));
    }
    private void initExercisesEditor()
    {
        Spinner sListOfExercises = vExercisesEditor.findViewById(R.id.sListOfExercises);
        sListOfExercises.setAdapter(initSList(TABLE_EXERCISES.TABLE_NAME, "Create new"));
        sListOfExercises.setOnItemSelectedListener(onItemSelectedListener());

        Spinner sListOfMuscleGroups = vExercisesEditor.findViewById(R.id.sListOfMuscleGroups);
        sListOfMuscleGroups.setAdapter(initSList(TABLE_MUSCLEGROUPS.TABLE_NAME, null));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.btnEdit:
                for (Map.Entry<Integer, Boolean> entry : stateOfEditors.entrySet())
                {
                    if (entry.getKey() == R.layout.exercises_editor_view && entry.getValue())
                    {
                        saveExerciseData();
                        Spinner sListOfExercises = vExercisesEditor.findViewById(R.id.sListOfExercises);
                        sListOfExercises.setAdapter(initSList(TABLE_EXERCISES.TABLE_NAME, "Create new"));
                    }
                    else if (entry.getKey() == R.layout.muscle_groups_editor_view && entry.getValue())
                    {
                        saveMuscleGroupData();
                        Spinner sMuscleGroupsList = vMuscleGroupsEditor.findViewById(R.id.sMuscleGroupsList);
                        sMuscleGroupsList.setAdapter(initSList(TABLE_MUSCLEGROUPS.TABLE_NAME, "Create new"));
                    }
                }

                break;
            case R.id.tvExercisesSwitch:
                tvExercisesSwitch.setBackgroundResource(R.drawable.rounded_corner_on);
                tvMuscleGroupsSwitch.setBackgroundResource(R.drawable.rounded_corner_off);
                clEditor.removeAllViews();
                Spinner sListOfMuscleGroups = vExercisesEditor.findViewById(R.id.sListOfMuscleGroups);
                sListOfMuscleGroups.setAdapter(initSList(TABLE_MUSCLEGROUPS.TABLE_NAME, null));
                if (stateOfButtonExercisesEditor) btnEdit.setText("Edit");
                else btnEdit.setText("Create");
                stateOfEditors.put(R.layout.exercises_editor_view, true);
                stateOfEditors.put(R.layout.muscle_groups_editor_view, false);
                clEditor.addView(vExercisesEditor, 0);
                break;
            case R.id.tvMuscleGroupsSwitch:
                tvMuscleGroupsSwitch.setBackgroundResource(R.drawable.rounded_corner_on);
                tvExercisesSwitch.setBackgroundResource(R.drawable.rounded_corner_off);
                clEditor.removeAllViews();
                if (stateOfButtonMuscleGroupsEditor) btnEdit.setText("Edit");
                else btnEdit.setText("Create");
                stateOfEditors.put(R.layout.muscle_groups_editor_view, true);
                stateOfEditors.put(R.layout.exercises_editor_view, false);
                clEditor.addView(vMuscleGroupsEditor, 0);
                break;
            default:
                break;
        }
    }

    AdapterView.OnItemSelectedListener onItemSelectedListener()
    {
        return new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                for (Map.Entry<Integer, Boolean> entry : stateOfEditors.entrySet())
                {
                    if (entry.getKey() == R.layout.exercises_editor_view && entry.getValue())
                    {
                        OnItemSelectedExercisesEditor(parent, view, position, id);
                    }
                    else if (entry.getKey() == R.layout.muscle_groups_editor_view && entry.getValue())
                    {
                        OnItemSelectedMuscleGroupsEditor(parent, view, position, id);
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        };
    }
    private ArrayAdapter<String> initSList(String tableName, String firstItem)
    {
        ArrayList<String> arrayList = new ArrayList();
        if (firstItem != null) arrayList.add(firstItem);
        ArrayAdapter<String> autoSpinnerAdapter = new ArrayAdapter<>(EditorActivity.this, android.R.layout.simple_spinner_item, arrayList);
        autoSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        cursor = db.query(tableName, null, null, null, null, null, null);
        if (cursor.moveToFirst())
        {
            int nameIndex = cursor.getColumnIndex(TABLE_EXERCISES.KEY_NAME);
            do {
                arrayList.add(cursor.getString(nameIndex));

            } while (cursor.moveToNext());
        }
        cursor.close();
        return autoSpinnerAdapter;
    }

    private void OnItemSelectedExercisesEditor(AdapterView<?> parent, View view, int position, long id)
    {
        TextView tvExerciseId = vExercisesEditor.findViewById(R.id.tvExerciseId);
        EditText etExerciseName = vExercisesEditor.findViewById(R.id.etExerciseName);
        EditText etExerciseNote = vExercisesEditor.findViewById(R.id.etExerciseNote);
        Spinner sListOfMuscleGroups = vExercisesEditor.findViewById(R.id.sListOfMuscleGroups);
        cursor = db.query(TABLE_EXERCISES.TABLE_NAME, null, TABLE_EXERCISES.KEY_NAME + " == ?",
                new String[] {parent.getSelectedItem().toString()}, null, null, null);
        if (cursor.moveToFirst())
        {
            String str = cursor.getString(cursor.getColumnIndex(TABLE_EXERCISES.KEY_MUSCLEGROUPID));
            tvExerciseId.setText(cursor.getString(cursor.getColumnIndex(TABLE_EXERCISES.KEY_ID)));
            etExerciseName.setText(cursor.getString(cursor.getColumnIndex(TABLE_EXERCISES.KEY_NAME)));
            etExerciseNote.setText(cursor.getString(cursor.getColumnIndex(TABLE_EXERCISES.KEY_INFO)));
            Cursor cursorMuscleGroups = db.query(TABLE_MUSCLEGROUPS.TABLE_NAME, null, TABLE_MUSCLEGROUPS.KEY_ID + " == ?",
                    new String[] {str}, null, null, null);
            if (cursorMuscleGroups.moveToFirst())
            {
                sListOfMuscleGroups.setSelection(initSList(TABLE_MUSCLEGROUPS.TABLE_NAME, null).getPosition(
                        cursorMuscleGroups.getString(cursorMuscleGroups.getColumnIndex(TABLE_MUSCLEGROUPS.KEY_NAME))));
            }


            btnEdit.setText("Edit");
            stateOfButtonExercisesEditor = true;
            cursorMuscleGroups.close();
        }
        else
        {
            tvExerciseId.setText("id");
            etExerciseName.setText("");
            etExerciseNote.setText("");
            btnEdit.setText("Create");
            stateOfButtonExercisesEditor = false;
        }
        cursor.close();
    }
    private  void OnItemSelectedMuscleGroupsEditor(AdapterView<?> parent, View view, int position, long id)
    {
        EditText etMuscleGroupName = vMuscleGroupsEditor.findViewById(R.id.etMuscleGroupName);
        Spinner sMajorMuscleGroupsList = vMuscleGroupsEditor.findViewById(R.id.sMajorMuscleGroupsList);
        cursor = db.query(TABLE_MUSCLEGROUPS.TABLE_NAME, null, TABLE_MUSCLEGROUPS.KEY_NAME + " == ?",
                new String[] {parent.getSelectedItem().toString()}, null, null, null);
        if (cursor.moveToFirst())
        {
            etMuscleGroupName.setText(cursor.getString(cursor.getColumnIndex(TABLE_MUSCLEGROUPS.KEY_NAME)));
            Cursor cursorMajorMuscleGroups = db.query(TABLE_MAJORMUSCLEGROUPS.TABLE_NAME, null, TABLE_MAJORMUSCLEGROUPS.KEY_ID + " == ?",
                    new String[] {cursor.getString(cursor.getColumnIndex(TABLE_MUSCLEGROUPS.KEY_MAJORMUSCLEGROUPID))}, null, null, null);
            if (cursorMajorMuscleGroups.moveToFirst())
            {
                sMajorMuscleGroupsList.setSelection(initSList(TABLE_MAJORMUSCLEGROUPS.TABLE_NAME, null).getPosition(
                        cursorMajorMuscleGroups.getString(cursorMajorMuscleGroups.getColumnIndex(TABLE_MUSCLEGROUPS.KEY_NAME))));
            }
            btnEdit.setText("Edit");
            stateOfButtonMuscleGroupsEditor = true;
            cursorMajorMuscleGroups.close();
        }
        else
        {
            etMuscleGroupName.setText("");
            btnEdit.setText("Create");
            stateOfButtonMuscleGroupsEditor = false;
        }

    }
    private void saveExerciseData()
    {
        EditText etExerciseName = vExercisesEditor.findViewById(R.id.etExerciseName);
        EditText etExerciseNote = vExercisesEditor.findViewById(R.id.etExerciseNote);
        ContentValues exerciseFields = new ContentValues();
        Spinner sListOfExercises = vExercisesEditor.findViewById(R.id.sListOfExercises);
        Spinner sListOfMuscleGroups = vExercisesEditor.findViewById(R.id.sListOfMuscleGroups);

        if (etExerciseName.getText().toString().equals(""))
        {
            Toast.makeText(EditorActivity.this, "Field <Exercise name> is empty.\nPlease input name.", Toast.LENGTH_SHORT).show();
            return;
        }
        cursor = db.query(TABLE_MUSCLEGROUPS.TABLE_NAME, null, TABLE_MUSCLEGROUPS.KEY_NAME + " == ?",
                new String[]{sListOfMuscleGroups.getSelectedItem().toString()}, null, null, null);
        if (!cursor.moveToFirst())
        {
            Toast.makeText(EditorActivity.this, "An error occurred: The selected muscle group could not be found.\nPlease try again", Toast.LENGTH_SHORT).show();
            return;
        }
        exerciseFields.clear();
        exerciseFields.put(TABLE_EXERCISES.KEY_NAME, etExerciseName.getText().toString());
        exerciseFields.put(TABLE_EXERCISES.KEY_MUSCLEGROUPID, cursor.getInt(cursor.getColumnIndex(TABLE_MUSCLEGROUPS.KEY_ID)));
        exerciseFields.put(TABLE_EXERCISES.KEY_INFO, etExerciseNote.getText().toString());
        if (stateOfButtonExercisesEditor) // Edit == update
        {
            db.update(TABLE_EXERCISES.TABLE_NAME, exerciseFields, TABLE_EXERCISES.KEY_NAME + "= ?", new String[] {sListOfExercises.getSelectedItem().toString()});
            Toast.makeText(EditorActivity.this, "Editing exercise completed successfully", Toast.LENGTH_SHORT).show();
        }
        else { // Create == insert
            db.insert(TABLE_EXERCISES.TABLE_NAME, null, exerciseFields);
            Toast.makeText(EditorActivity.this, "Creation exercise completed successfully", Toast.LENGTH_SHORT).show();
        }

    }
    private void saveMuscleGroupData()
    {
        EditText etMuscleGroupName = findViewById(R.id.etMuscleGroupName);
        Spinner sMuscleGroupsList = vMuscleGroupsEditor.findViewById(R.id.sMuscleGroupsList);
        Spinner sMajorMuscleGroupsList = vMuscleGroupsEditor.findViewById(R.id.sMajorMuscleGroupsList);
        ContentValues muscleGroupFields = new ContentValues();
        if (etMuscleGroupName.getText().toString().equals(""))
        {
            Toast.makeText(EditorActivity.this, "Field <Muscle group name> is empty.\nPlease input name.", Toast.LENGTH_SHORT).show();
            return;
        }

        muscleGroupFields.put(TABLE_MUSCLEGROUPS.KEY_NAME, etMuscleGroupName.getText().toString());
        cursor = db.query(TABLE_MAJORMUSCLEGROUPS.TABLE_NAME, null, TABLE_MAJORMUSCLEGROUPS.KEY_NAME + " == ?",
                new String[]{sMajorMuscleGroupsList.getSelectedItem().toString()}, null, null, null);
        if (!cursor.moveToFirst())
        {
            Toast.makeText(EditorActivity.this, "An error occurred: The selected major muscle group could not be found.\nPlease try again", Toast.LENGTH_SHORT).show();
            return;
        }
        muscleGroupFields.put(TABLE_MUSCLEGROUPS.KEY_MAJORMUSCLEGROUPID, cursor.getInt(cursor.getColumnIndex(TABLE_MAJORMUSCLEGROUPS.KEY_ID)));
        if (stateOfButtonMuscleGroupsEditor) // Edit == update
        {
            db.update(TABLE_MUSCLEGROUPS.TABLE_NAME, muscleGroupFields, TABLE_MUSCLEGROUPS.KEY_NAME + "= ?", new String[] {sMuscleGroupsList.getSelectedItem().toString()});
            Toast.makeText(EditorActivity.this, "Editing muscle group completed successfully", Toast.LENGTH_SHORT).show();
        }
        else { // Create == insert
            db.insert(TABLE_MUSCLEGROUPS.TABLE_NAME, null, muscleGroupFields);
            Toast.makeText(EditorActivity.this, "Creation muscle group completed successfully", Toast.LENGTH_SHORT).show();
        }


    }


}