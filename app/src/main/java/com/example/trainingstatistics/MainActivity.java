package com.example.trainingstatistics;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    Button btnCalendar, btnTestActivity, btnExerciseEditor, btnMuscleGroupsEditor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnCalendar = (Button) findViewById(R.id.btnCalendar);
        btnCalendar.setOnClickListener(this);

        btnTestActivity = (Button) findViewById(R.id.btnTestActivity);
        btnTestActivity.setOnClickListener(this);

        btnExerciseEditor = (Button) findViewById(R.id.btnExerciseEditor);
        btnExerciseEditor.setOnClickListener(this);

    }

    @Override
    public void onClick(View v)
    {
        Intent intent;
        switch (v.getId())
        {
            case R.id.btnCalendar:
                intent = new Intent(this, CalendarActivity.class);
                startActivity(intent);
                break;
            case R.id.btnTestActivity:
                intent = new Intent(this, TestActivity.class);
                startActivity(intent);
                break;
            case R.id.btnExerciseEditor:
                intent = new Intent(this, EditorActivity.class);
                startActivity(intent);
                break;
            default:
                break;
        }
    }
}