package com.example.trainingstatistics;

public class TABLE_COMPLEXES {
    public static final String TABLE_NAME = "Complexes";
    public static final String KEY_ID = "_id";
    public static final String KEY_COMPLEXINDEX = "complexIndex";
    public static final String KEY_NAME = "name";
    public static final String KEY_COUNT = "count";
    public static final String KEY_TRAININGID = "trainingId";
}