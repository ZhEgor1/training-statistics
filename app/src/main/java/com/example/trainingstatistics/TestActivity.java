package com.example.trainingstatistics;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;
import com.riontech.calendar.CustomCalendar;
import com.riontech.calendar.dao.EventData;
import com.riontech.calendar.dao.dataAboutDate;
import com.riontech.calendar.utils.CalendarUtils;

//import org.naishadhparmar.zcustomcalendar.CustomCalendar;
//import org.naishadhparmar.zcustomcalendar.OnDateSelectedListener;
//import org.naishadhparmar.zcustomcalendar.Property;


import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;


public class TestActivity extends AppCompatActivity {
    private CustomCalendar customCalendar;
    Calendar calendar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);

        customCalendar = (CustomCalendar) findViewById(R.id.customCalendar);

        String[] arr = {"2021-08-10", "2021-08-11", "2021-08-15", "2021-08-16", "2021-08-25"};

        for (int i = 0; i < arr.length; i++) {

            int eventCount = 3;

            customCalendar.addAnEvent(arr[i], eventCount, getEventDataList(eventCount));

        }
//        customCalendar = findViewById(R.id.custom_calendar);
////        customCalendar.setDayOfWeekStartFrom(0);
////        customCalendar.setDayOfWeekLength(1);
//        HashMap<Object, Property> descHashMap = new HashMap<>();
//
//        Property defaultProperty = new Property();
//        defaultProperty.layoutResource = R.layout.default_view;
//        defaultProperty.dateTextViewResource = R.id.text_view;
//        descHashMap.put("default", defaultProperty);
//
//        Property currentProperty = new Property();
//        currentProperty.layoutResource = R.layout.current_view;
//        currentProperty.dateTextViewResource = R.id.text_view;
//        descHashMap.put("current", currentProperty);
//
//        Property presentProperty = new Property();
//        presentProperty.layoutResource = R.layout.present_view;
//        presentProperty.dateTextViewResource = R.id.text_view;
//        descHashMap.put("present", presentProperty);
//
//        Property absentProperty = new Property();
//        absentProperty.layoutResource = R.layout.absent_view;
//        absentProperty.dateTextViewResource = R.id.text_view;
//        descHashMap.put("absent", absentProperty);
//
//        customCalendar.setMapDescToProp(descHashMap);
//
//        HashMap<Integer, Object> dateHashMap = new HashMap<>();
//
//        Calendar calendar = Calendar.getInstance();
//
//        dateHashMap.put(calendar.get(Calendar.DAY_OF_MONTH), "current");
//        dateHashMap.put(1, "present");
//        dateHashMap.put(2, "absent");
//        dateHashMap.put(3, "present");
//        dateHashMap.put(4, "absent");
//        dateHashMap.put(20, "present");
//
//        customCalendar.setDate(calendar, dateHashMap);
//        customCalendar.setOnNavigationButtonClickedListener(CustomCalendar.PREVIOUS, );
//        customCalendar.setOnNavigationButtonClickedListener(CustomCalendar.NEXT, );
//        customCalendar.setOnDateSelectedListener(new OnDateSelectedListener() {
//            @Override
//            public void onDateSelected(View view, Calendar selectedDate, Object desc) {
//                Snackbar.make(customCalendar, selectedDate.get(Calendar.DAY_OF_MONTH) + " selected", Snackbar.LENGTH_LONG).show();
//            }
//        });
    }
//        customCalendar.setOnDateSelectedListener(new OnDateSelectedListener() {
//            @Override
//            public void onDateSelected(View view, Calendar selectedDate, Object desc) {
//                String sDate = selectedDate.get(Calendar.DAY_OF_MONTH)
//                        + "/" + selectedDate.get(Calendar.MONTH)
//                        + "/" + selectedDate.get(Calendar.YEAR);
//
//                Toast.makeText(getApplicationContext(), sDate, Toast.LENGTH_SHORT).show();
//            }
//        });

    public ArrayList<EventData> getEventDataList(int count) {
        ArrayList<EventData> eventDataList = new ArrayList();

        EventData dateData = new EventData();
        ArrayList<dataAboutDate> dataAboutDates = new ArrayList();

        dateData.setSection("Sample of training");
        dataAboutDate dataAboutDate = new dataAboutDate();


        dataAboutDate.setTitle("Sample #");
        dataAboutDate.setSubject("Description simple #");
        dataAboutDates.add(dataAboutDate);

        dateData.setData(dataAboutDates);
        eventDataList.add(dateData);

        return eventDataList;
    }

}