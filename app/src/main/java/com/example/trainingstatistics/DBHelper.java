package com.example.trainingstatistics;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;



public class DBHelper extends SQLiteOpenHelper
{
    public static final String DB_NAME = "trainingStatisticsDB";
    public static final int DB_VERSION = 1;


    public DBHelper(@Nullable Context context)
    {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db)
    {
//        db.execSQL("create table " + TABLE_USERS.TABLE_NAME + "(" + TABLE_USERS.KEY_ID
//                + " integer primary key autoincrement," + TABLE_USERS.KEY_NAME + " text," + TABLE_USERS.KEY_SURNAME
//                + " text," + TABLE_USERS.KEY_EMAIL + " text," + TABLE_USERS.KEY_DATEOFBIRTH + " text,"
//                + TABLE_USERS.KEY_GENDER + " text" + ")"); // Users_table

        db.execSQL("create table " + TABLE_MAJORMUSCLEGROUPS.TABLE_NAME + " (" + TABLE_MAJORMUSCLEGROUPS.KEY_ID
                + " integer primary key autoincrement," + TABLE_MAJORMUSCLEGROUPS.KEY_NAME + " text" + ")"); // majorMuscleGroups table

        db.execSQL("create table " + TABLE_MUSCLEGROUPS.TABLE_NAME + " (" + TABLE_MUSCLEGROUPS.KEY_ID
                + " integer primary key autoincrement," + TABLE_MUSCLEGROUPS.KEY_NAME + " text,"
                + TABLE_MUSCLEGROUPS.KEY_MAJORMUSCLEGROUPID + " integer," + "foreign key("
                + TABLE_MUSCLEGROUPS.KEY_MAJORMUSCLEGROUPID + ") references " + TABLE_MAJORMUSCLEGROUPS.TABLE_NAME
                + "(" + TABLE_MAJORMUSCLEGROUPS.KEY_ID + ")" + ")"); // MuscleGroups table

        db.execSQL("create table " + TABLE_EXERCISES.TABLE_NAME + "(" + TABLE_EXERCISES.KEY_ID
                + " integer primary key autoincrement," + TABLE_EXERCISES.KEY_NAME + " text," + TABLE_EXERCISES.KEY_MUSCLEGROUPID
                + " integer," + TABLE_EXERCISES.KEY_INFO + " text," + "foreign key("
                + TABLE_EXERCISES.KEY_MUSCLEGROUPID + ") references " + TABLE_MUSCLEGROUPS.TABLE_NAME
                + "(" + TABLE_MUSCLEGROUPS.KEY_ID + ")" + ")"); // Exercises table

//        db.execSQL("create table " + TABLE_TRAININGS.TABLE_NAME + " (" + TABLE_TRAININGS.KEY_ID
//                + " integer primary key autoincrement," + TABLE_TRAININGS.KEY_TYPE + " text,"
//                + TABLE_TRAININGS.KEY_DATE + " integer," + TABLE_TRAININGS.KEY_USERID + " integer not null,"
//                + "foreign key(" + TABLE_TRAININGS.KEY_USERID + ") references " + TABLE_USERS.TABLE_NAME
//                + "(" + TABLE_USERS.KEY_ID + ")" + ")"); //Trainings table

        db.execSQL("create table " + TABLE_TRAININGS.TABLE_NAME + " (" + TABLE_TRAININGS.KEY_ID
                + " integer primary key autoincrement," + TABLE_TRAININGS.TABLE_NAME + " text,"
                + TABLE_TRAININGS.KEY_NAME + " text," + TABLE_TRAININGS.KEY_NOTE + " text,"
                + TABLE_TRAININGS.KEY_DATE + " integer," + TABLE_TRAININGS.KEY_USERID + " integer" + ")"); //Trainings table

        db.execSQL("create table " + TABLE_MAJORMUSCLEGROUPSLIST.TABLE_NAME + "(" + TABLE_MAJORMUSCLEGROUPSLIST.KEY_MAJORMUSCLEGROUPID
                + " integer," + TABLE_MAJORMUSCLEGROUPSLIST.KEY_TRAININGID + " integer," + "foreign key("
                + TABLE_MAJORMUSCLEGROUPSLIST.KEY_MAJORMUSCLEGROUPID + ") references " + TABLE_MAJORMUSCLEGROUPS.TABLE_NAME
                + "(" + TABLE_MAJORMUSCLEGROUPS.KEY_ID + ")," + "foreign key(" + TABLE_MAJORMUSCLEGROUPSLIST.KEY_TRAININGID
                + ") references " + TABLE_TRAININGS.TABLE_NAME + "(" + TABLE_TRAININGS.KEY_ID + ")" +")"); // MajorMuscleGroupsList table

        db.execSQL("create table " + TABLE_COMPLEXES.TABLE_NAME + " (" + TABLE_COMPLEXES.KEY_ID
                + " integer primary key autoincrement," + TABLE_COMPLEXES.KEY_COMPLEXINDEX + " integer,"
                + TABLE_COMPLEXES.KEY_NAME + " text," + TABLE_COMPLEXES.KEY_COUNT
                + " integer," + TABLE_COMPLEXES.KEY_TRAININGID + " integer not null,"
                + "foreign key(" + TABLE_COMPLEXES.KEY_TRAININGID + ") references "
                + TABLE_TRAININGS.TABLE_NAME + "(" + TABLE_TRAININGS.KEY_ID + ")" + ")"); // Complexes table

        db.execSQL("create table " + TABLE_PATTERNS.TABLE_NAME + " (" + TABLE_PATTERNS.KEY_ID
                + " integer primary key autoincrement," + TABLE_PATTERNS.KEY_NAME
                + " text," + TABLE_PATTERNS.KEY_TRAINIGID + " integer not null," + "foreign key("
                + TABLE_PATTERNS.KEY_TRAINIGID + ") references " + TABLE_TRAININGS.TABLE_NAME
                + "(" + TABLE_TRAININGS.KEY_ID + ")" + ")"); // Patterns table

        db.execSQL("create table " + TABLE_SETS.TABLE_NAME + " (" + TABLE_SETS.KEY_SETINDEX
                + " integer not null," + TABLE_SETS.KEY_EXERCISESID + " integer not null,"
                + TABLE_SETS.KEY_COMPLEXID + " integer not null," + TABLE_SETS.KEY_REPSLEFT
                + " integer," + TABLE_SETS.KEY_REPSRIGHT + " integer,"+ TABLE_SETS.KEY_WEIGHT
                + " real," + TABLE_SETS.KEY_NOTE + " text," + "foreign key(" + TABLE_SETS.KEY_EXERCISESID
                + ") references " + TABLE_EXERCISES.TABLE_NAME + "(" + TABLE_EXERCISES.KEY_ID + "),"
                + "foreign key(" + TABLE_SETS.KEY_COMPLEXID + ") references "
                + TABLE_COMPLEXES.TABLE_NAME + "(" + TABLE_COMPLEXES.KEY_ID + ")" + ")"); // Sets table

        String[] arrMajorMuscleGroups = {"chest", "back", "arms", "abdominals", "legs", "shoulders"};
        for (int i = 0; i < 6; i++)
        {
            ContentValues cvMajorMuscleGroup = new ContentValues();
            cvMajorMuscleGroup.put(TABLE_MAJORMUSCLEGROUPS.KEY_NAME, arrMajorMuscleGroups[i]);
            db.insert(TABLE_MAJORMUSCLEGROUPS.TABLE_NAME, null, cvMajorMuscleGroup);
        }

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
    {
        if (oldVersion < newVersion)
        {
            // drop database
        }
        onCreate(db);
    }
}
