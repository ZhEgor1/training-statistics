package com.example.trainingstatistics;

public class TABLE_MUSCLEGROUPS {
    public static final String TABLE_NAME = "MuscleGroups";
    public static final String KEY_ID = "_id";
    public static final String KEY_NAME = "name";
    public static final String KEY_MAJORMUSCLEGROUPID = "majorMuscleGroupId";

}
