package com.example.trainingstatistics;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


public class CalendarActivity extends AppCompatActivity implements View.OnClickListener {


    CalendarView calendarV;
    Cursor cursor = null;
    TextView tvDate, tvSampleWorkout;
    Button btnApprove;
    DBHelper dbHelper;
    Date date;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calendar);

        calendarV = findViewById(R.id.calendarView);
        calendarV.setFirstDayOfWeek(2);

        tvDate = findViewById(R.id.tvSelectedDate);
        tvSampleWorkout = findViewById(R.id.tvSampleWorkout);

        btnApprove = findViewById(R.id.btnApprove);
        btnApprove.setOnClickListener(this);

        dbHelper = new DBHelper(this);
        SQLiteDatabase db = dbHelper.getReadableDatabase();

        calendarV.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(@NonNull CalendarView view, int year, int month, int dayOfMonth) {
//                longDate = (view.getDate()/86400000)*86400000;// rounding to days
                month += 1;
                SimpleDateFormat dateInputFormat = new SimpleDateFormat("yyyy/M/dd");
                SimpleDateFormat dateOutputFormat = new SimpleDateFormat("EEE, MMM d, yyyy");

                try {
                    date = dateInputFormat.parse(year + "/" + month + "/" + dayOfMonth);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                btnApprove.setEnabled(true);

                // Запрос в базу данных по дате.
                cursor = db.query(TABLE_TRAININGS.TABLE_NAME, null, TABLE_TRAININGS.KEY_DATE + " = ?", new String[] {date.getTime()/86400000 + ""}, null, null, null);
                String dayInfo;
                if (cursor != null)
                {
                    if (cursor.moveToFirst())
                    {
                        int nameIndex = cursor.getColumnIndex(TABLE_TRAININGS.KEY_NAME);
                        dayInfo = "\n" + cursor.getString(nameIndex);
                    }
                    else
                    {
                        dayInfo = "\ninfo not found";
                    }
                }
                else
                {
                    dayInfo = "\ncursor not found";
                }

                tvDate.setText(dateOutputFormat.format(date));
                tvSampleWorkout.setText(dayInfo);
            }
        });
    }

    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.btnApprove:
//                  Проверка на пустую дату сделать.
                Intent intent = new Intent(this, DayInfoActivity.class);
                intent.putExtra("date", date.getTime());
                startActivity(intent);
                break;
            default:
                break;
        }
    }

}