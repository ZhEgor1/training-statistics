package com.example.trainingstatistics;

public class TABLE_USERS {
    public static final String TABLE_NAME = "Users";
    public static final String KEY_ID = "_id";
    public static final String KEY_NAME = "name";
    public static final String KEY_SURNAME = "surname";
    public static final String KEY_EMAIL = "email";
    public static final String KEY_DATEOFBIRTH = "dateOfBirth";
    public static final String KEY_GENDER = "gender";
}