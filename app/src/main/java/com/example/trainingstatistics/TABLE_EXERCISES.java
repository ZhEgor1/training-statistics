package com.example.trainingstatistics;

public class TABLE_EXERCISES {
    public static final String TABLE_NAME = "Exercises";
    public static final String KEY_ID = "_id";
    public static final String KEY_NAME = "name";
    public static final String KEY_MUSCLEGROUPID = "muscleGroupId";
    public static final String KEY_INFO = "info";
}