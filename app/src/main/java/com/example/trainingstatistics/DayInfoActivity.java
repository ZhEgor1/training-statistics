package com.example.trainingstatistics;

import androidx.appcompat.app.AppCompatActivity;

import android.animation.LayoutTransition;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;

import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;


public class DayInfoActivity extends AppCompatActivity implements View.OnClickListener {

    TextView tvDate;

    Intent incomingIntent;

    Cursor cursor = null;
    LinearLayout llvComplexContainer;
    ArrayAdapter<String> autoExerciseAdapter;
    EditText etTrainingName;
    Button btnAdd, btnSave;
    DBHelper dbHelper;
    ArrayList<String> listOfExercises;
    SQLiteDatabase db;
    Date date;
    SimpleDateFormat dateOutputFormat;
    TableLayout tlTags;
    boolean dataChanged;
    int countSets;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_day_info);

        btnAdd = findViewById(R.id.btnAdd);
        btnAdd.setOnClickListener(this);

        btnSave = findViewById(R.id.btnSave);
        btnSave.setOnClickListener(this);

        etTrainingName = findViewById(R.id.etTrainingName);
        etTrainingName.addTextChangedListener(textWatcher);

        llvComplexContainer = findViewById(R.id.llvComplexContainer);
        LayoutTransition transition = new LayoutTransition();
        llvComplexContainer.setLayoutTransition(transition);

        tlTags = findViewById(R.id.tlTags);

        dbHelper = new DBHelper(this);
        db = dbHelper.getWritableDatabase();

        incomingIntent = getIntent();
        date = new Date();
        date.setTime(incomingIntent.getLongExtra("date", 0));
        dateOutputFormat = new SimpleDateFormat("EEE, MMM d");
        tvDate = findViewById(R.id.tvDate);
        tvDate.setText(dateOutputFormat.format(date));

        listOfExercises = new ArrayList<>();
        initSListOfExercises();
        autoExerciseAdapter = new ArrayAdapter<>(DayInfoActivity.this, android.R.layout.simple_spinner_item, listOfExercises);
        autoExerciseAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        countSets = 0;
        initDayFields();
        dataChanged = false;
    }

    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.btnAdd:
                addComplex();
                break;
            case R.id.btnSave:
                updateDayData();
                break;
            default:
                break;
        }
    }

    private TextWatcher textWatcher = new TextWatcher() {

        public void afterTextChanged(Editable s) {
            dataChanged = true;
        }

        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }
    };
    @Override
    public void onBackPressed()
    {
        if (dataChanged)
        {
            new AlertDialog.Builder(this)
//                .setIcon(android.R.drawable.ic_dialog_alert)
                    .setTitle("Closing Activity")
                    .setMessage("You didn't save the data, do you want to save it now?")
                    .setPositiveButton("Yes, save", new DialogInterface.OnClickListener()
                    {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            updateDayData();
                            finish();
                        }

                    })
                    .setNegativeButton("No, don't save", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    })
                    .setNeutralButton("Cancel", null)
                    .show();
        }
        else super.onBackPressed();
    }

    private void addComplex()
    {
        dataChanged = true;
        LayoutInflater layoutInflater = (LayoutInflater) getBaseContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View vAddComplex = layoutInflater.inflate(R.layout.complexes_row, null);
        final EditText etComplexName = vAddComplex.findViewById(R.id.etComplexName);
        etComplexName.setText("Complex name");

        Button btnDeleteComplex = vAddComplex.findViewById(R.id.btnDeleteComplex);// remove complex
        btnDeleteComplex.setOnClickListener(v -> ((LinearLayout)vAddComplex.getParent()).removeView(vAddComplex));

        addSet(vAddComplex);
        llvComplexContainer.addView(vAddComplex, 0);
    }
    private void addSet(View vAddComplex)
    {
        Button btnAddSet = vAddComplex.findViewById(R.id.btnAddSet); //add set
        btnAddSet.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                LayoutInflater layoutInflater = (LayoutInflater) getBaseContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                LinearLayout llvSetContainer = vAddComplex.findViewById(R.id.llvSetContainer);
                LayoutTransition transition = new LayoutTransition();
                llvSetContainer.setLayoutTransition(transition);
                final View vAddSet = layoutInflater.inflate(R.layout.sets_row, null);

                Spinner sExercises = vAddSet.findViewById(R.id.sExercises);
                sExercises.setAdapter(autoExerciseAdapter);
                sExercises.setOnItemSelectedListener(onItemSelectedListener());
                EditText etnLeftReps = vAddSet.findViewById(R.id.etnReps);
                EditText etnWeight = vAddSet.findViewById(R.id.etnWeight);
                TextView tvSetIndex = vAddSet.findViewById(R.id.tvSetIndex);
                etnLeftReps.addTextChangedListener(textWatcher);
                etnWeight.addTextChangedListener(textWatcher);
                int currentSetIndex = 1;
                if (llvSetContainer.getChildCount() >= 1)
                {
                    TextView tvPreviousIndex = llvSetContainer.getChildAt(llvSetContainer.getChildCount() - 1).findViewById(R.id.tvSetIndex);
                    Spinner sPreviousExercise = llvSetContainer.getChildAt(llvSetContainer.getChildCount() - 1).findViewById(R.id.sExercises);
                    sExercises.setSelection(sPreviousExercise.getSelectedItemPosition());
                    currentSetIndex = Integer.parseInt(tvPreviousIndex.getTag().toString()) + 1;

                }
                tvSetIndex.setText(currentSetIndex + ". ");
                tvSetIndex.setTag(currentSetIndex);


                Button btnDeleteSet = vAddSet.findViewById(R.id.btnDeleteSet);
                btnDeleteSet.setOnClickListener(v1 -> {
                    ((LinearLayout) vAddSet.getParent()).removeView(vAddSet);
                    dataChanged = true;
                });

                Button btnGetNote = vAddSet.findViewById(R.id.btnGetNote);
                btnGetNote.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // get note from the db
                    }
                });
                llvSetContainer.addView(vAddSet, -1);
                dataChanged = true;
            }
        });
        dataChanged = true;
    }
    private void saveDayData()
    {
        ContentValues trainingFields = new ContentValues();
        int complexCount = llvComplexContainer.getChildCount();
        trainingFields.put(TABLE_TRAININGS.KEY_NAME, etTrainingName.getText().toString());
        trainingFields.put(TABLE_TRAININGS.KEY_DATE, date.getTime()/86400000);
        db.insert(TABLE_TRAININGS.TABLE_NAME, null, trainingFields);
        cursor = db.query(TABLE_TRAININGS.TABLE_NAME, null, TABLE_TRAININGS.KEY_DATE + " == ?", new String[]{String.valueOf(date.getTime()/86400000)}, null, null, null);

        if (!cursor.moveToFirst())
        {
            Toast.makeText(DayInfoActivity.this, "Failure to create", Toast.LENGTH_SHORT).show();
            cursor.close();
            return;
        }
        int idIndex = cursor.getColumnIndex(TABLE_TRAININGS.KEY_ID);
        int trainingId = cursor.getInt(idIndex);
        for (int i = 0; i < complexCount; i++)
        {

            ContentValues complexFields = new ContentValues();
            View complexView = llvComplexContainer.getChildAt(i);
            LinearLayout llvSetContainer = complexView.findViewById(R.id.llvSetContainer);
            int setCount = llvSetContainer.getChildCount();
            EditText etComplexName = complexView.findViewById(R.id.etComplexName);
            complexFields.put(TABLE_COMPLEXES.KEY_NAME, etComplexName.getText().toString());
            complexFields.put(TABLE_COMPLEXES.KEY_COUNT, setCount);
            complexFields.put(TABLE_COMPLEXES.KEY_TRAININGID, trainingId);
            complexFields.put(TABLE_COMPLEXES.KEY_COMPLEXINDEX, complexCount - i);

            db.insert(TABLE_COMPLEXES.TABLE_NAME, null, complexFields);
            Cursor cursorComplex = db.query(TABLE_COMPLEXES.TABLE_NAME, null, TABLE_COMPLEXES.KEY_TRAININGID + " == ?",
                    new String[] {String.valueOf(trainingId)}, null, null, TABLE_COMPLEXES.KEY_COMPLEXINDEX);
            if (!cursorComplex.moveToFirst()) continue;

            int complexIdIndex = cursorComplex.getColumnIndex(TABLE_COMPLEXES.KEY_ID);
            int complexId = cursorComplex.getInt(complexIdIndex);

            for (int j = 0; j < setCount; j++)
            {
                ContentValues setFields = new ContentValues();

                View setView = llvSetContainer.getChildAt(j);
                Spinner sExercises = setView.findViewById(R.id.sExercises);
                EditText etnReps = setView.findViewById(R.id.etnReps);
                EditText etnWeight = setView.findViewById(R.id.etnWeight);

                Cursor cursorExercise = db.query(TABLE_EXERCISES.TABLE_NAME, null, TABLE_EXERCISES.KEY_NAME + " == ?",
                        new String[]{sExercises.getSelectedItem().toString()}, null, null, null);

                if (!cursorExercise.moveToFirst()) continue;
                int exerciseIdIndex = cursorExercise.getColumnIndex(TABLE_EXERCISES.KEY_ID);

                String commonReps = etnReps.getText().toString();
                String leftReps = "";
                String rightReps = "";
                boolean RightRepsExists = false;
                for (int cIndex = 0; cIndex < commonReps.length(); cIndex++)
                {
                    if (commonReps.charAt(cIndex) == ':' || RightRepsExists)
                    {
                        if (commonReps.charAt(cIndex) == ':')
                        {
                            RightRepsExists = true;
                            continue;
                        }
                        rightReps += commonReps.charAt(cIndex);
                    }
                    else
                    {
                        leftReps += commonReps.charAt(cIndex);
                    }
                }
                setFields.put(TABLE_SETS.KEY_REPSLEFT, leftReps);
                setFields.put(TABLE_SETS.KEY_REPSRIGHT, rightReps);
                setFields.put(TABLE_SETS.KEY_EXERCISESID, cursorExercise.getString(exerciseIdIndex));
                setFields.put(TABLE_SETS.KEY_WEIGHT, etnWeight.getText().toString());
                setFields.put(TABLE_SETS.KEY_SETINDEX, j + 1);
                setFields.put(TABLE_SETS.KEY_COMPLEXID, complexId);
                db.insert(TABLE_SETS.TABLE_NAME, null, setFields);
                cursorExercise.close();
            }
            cursorComplex.close();
        }
        cursor.close();
        dataChanged = false;
        Toast.makeText(DayInfoActivity.this, "Successfully saved", Toast.LENGTH_SHORT).show();
    }
    private void initTags(ArrayList<String> tags)
    {
        int countColumn = 3;
        int countRow = (int) Math.ceil(tags.size() / countColumn);
        LayoutInflater layoutInflater = (LayoutInflater) getBaseContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        for (int i = 0, z = 0; i < countRow; i++)
        {
            tlTags.addView(new TableRow(DayInfoActivity.this));
            if (countRow - 2 == i && tags.size() % countColumn != 0) countRow = tags.size() % countColumn;
            for (int j = 0; j < countColumn; j++, z++)
            {

                TableRow tr = (TableRow) tlTags.getChildAt(i);

                TableRow.LayoutParams paramsForTR = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT);
                paramsForTR.setMargins(0,4,0,4);
                tr.setLayoutParams(paramsForTR);

                tr.addView(layoutInflater.inflate(R.layout.tag_view, null));
                View vTag = tr.getChildAt(j);
                TextView tvTag = vTag.findViewById(R.id.tvMajorMuscleGroupTag);
                tvTag.setText(tags.get(z));
            }
        }
    }
    private void initDayFields()
    {
        cursor = db.query(TABLE_TRAININGS.TABLE_NAME, null, TABLE_TRAININGS.KEY_DATE + "== ?", new String[]{String.valueOf(date.getTime()/86400000)}, null, null, null);
        if (!cursor.moveToFirst())
        {
            Cursor cursorMajorMuscleGroups = db.query(TABLE_MAJORMUSCLEGROUPS.TABLE_NAME, null, null, null, null, null, null);
            if (cursorMajorMuscleGroups.moveToFirst())
            {
                ArrayList<String> tags = new ArrayList<>();
                do {
                       tags.add(cursorMajorMuscleGroups.getString(cursorMajorMuscleGroups.getColumnIndex(TABLE_MAJORMUSCLEGROUPS.KEY_NAME)));
                } while (cursorMajorMuscleGroups.moveToNext());
                initTags(tags);
            }
            cursorMajorMuscleGroups.close();
            cursor.close();
            return;
        }

        int trainingId = cursor.getInt(cursor.getColumnIndex(TABLE_TRAININGS.KEY_ID));
        etTrainingName.setText(cursor.getString(cursor.getColumnIndex(TABLE_TRAININGS.KEY_NAME)));
        cursor = db.query(TABLE_COMPLEXES.TABLE_NAME, null, TABLE_COMPLEXES.KEY_TRAININGID + " == ?",
                new String[] {String.valueOf(trainingId)}, null, null, null);
        if (cursor.moveToLast())
        {
            LayoutInflater layoutInflater = (LayoutInflater) getBaseContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            do {
                int complexIdIndex = cursor.getColumnIndex(TABLE_COMPLEXES.KEY_ID);

                final View vAddComplex = layoutInflater.inflate(R.layout.complexes_row, null);
                final EditText etComplexName = vAddComplex.findViewById(R.id.etComplexName);
                etComplexName.addTextChangedListener(textWatcher);
                etComplexName.setText(cursor.getString(cursor.getColumnIndex(TABLE_COMPLEXES.KEY_NAME)));
                Cursor cursorSet = db.query(TABLE_SETS.TABLE_NAME, null, TABLE_SETS.KEY_COMPLEXID + " == ?",
                        new String[] {cursor.getString(complexIdIndex)}, null, null, TABLE_SETS.KEY_SETINDEX);
                if (cursorSet.moveToLast())
                {
                    LinearLayout llvSetContainer = vAddComplex.findViewById(R.id.llvSetContainer);
                    LayoutTransition transition = new LayoutTransition();
                    llvSetContainer.setLayoutTransition(transition);
                    do {
                        final View vAddSet = layoutInflater.inflate(R.layout.sets_row, null);
                        countSets++;
                        Spinner sExercises = vAddSet.findViewById(R.id.sExercises);
                        EditText etnReps = vAddSet.findViewById(R.id.etnReps);
                        EditText etnWeight = vAddSet.findViewById(R.id.etnWeight);
                        TextView tvSetIndex = vAddSet.findViewById(R.id.tvSetIndex);
                        sExercises.setOnItemSelectedListener(onItemSelectedListener());
                        etnReps.addTextChangedListener(textWatcher);

                        etnWeight.addTextChangedListener(textWatcher);

                        String rightReps = cursorSet.getString(cursorSet.getColumnIndex(TABLE_SETS.KEY_REPSRIGHT));
                        if (!rightReps.equals("")) etnReps.setText(cursorSet.getString(cursorSet.getColumnIndex(TABLE_SETS.KEY_REPSLEFT)) + ":" + rightReps);
                        else etnReps.setText(cursorSet.getString(cursorSet.getColumnIndex(TABLE_SETS.KEY_REPSLEFT)));

                        etnWeight.setText(cursorSet.getString(cursorSet.getColumnIndex(TABLE_SETS.KEY_WEIGHT)));
                        tvSetIndex.setText(cursorSet.getString(cursorSet.getColumnIndex(TABLE_SETS.KEY_SETINDEX)) + ". ");
                        tvSetIndex.setTag(cursorSet.getString(cursorSet.getColumnIndex(TABLE_SETS.KEY_SETINDEX)));
                        sExercises.setAdapter(autoExerciseAdapter);

                        Cursor cursorExercise = db.query(TABLE_EXERCISES.TABLE_NAME, null, TABLE_EXERCISES.KEY_ID + " == ?",
                                new String[] {cursorSet.getString(cursorSet.getColumnIndex(TABLE_SETS.KEY_EXERCISESID))}, null, null, null);
                        if (cursorExercise.moveToFirst())
                        {
                            sExercises.setSelection(autoExerciseAdapter.getPosition(cursorExercise.getString(cursorExercise.getColumnIndex(TABLE_EXERCISES.KEY_NAME))));
                        }


                        llvSetContainer.addView(vAddSet, 0);

                        Button btnDeleteSet = vAddSet.findViewById(R.id.btnDeleteSet);
                        btnDeleteSet.setOnClickListener(new View.OnClickListener(){
                            @Override
                            public void onClick(View v) {
                                ((LinearLayout)vAddSet.getParent()).removeView(vAddSet);
                                dataChanged = true;

                            }
                        });

                        Button btnGetNote = vAddSet.findViewById(R.id.btnGetNote);
                        btnGetNote.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                // get note from the db
                            }
                        });
                    } while (cursorSet.moveToPrevious());
                }

                llvComplexContainer.addView(vAddComplex, 0);

                addSet(vAddComplex);

                Button btnDeleteComplex = vAddComplex.findViewById(R.id.btnDeleteComplex);
                btnDeleteComplex.setOnClickListener(new View.OnClickListener(){ // remove complex
                    @Override
                    public void onClick(View v) {
                        ((LinearLayout)vAddComplex.getParent()).removeView(vAddComplex);
                        dataChanged = true;
                    }});
            } while (cursor.moveToPrevious());
        }
        cursor.close();
    }
    private void deleteDayData()
    {
        String trainingId = cursor.getString(cursor.getColumnIndex(TABLE_TRAININGS.KEY_ID));
        Cursor cursorComplex = db.query(TABLE_COMPLEXES.TABLE_NAME, null, TABLE_COMPLEXES.KEY_TRAININGID + " == ?",
                new String[] {trainingId}, null, null, null);
        String deletedData = "";
        if (cursorComplex.moveToFirst())
        {
            do {
                String complexId = cursorComplex.getString(cursorComplex.getColumnIndex(TABLE_COMPLEXES.KEY_ID));
                Cursor cursorSet = db.query(TABLE_SETS.TABLE_NAME, null, TABLE_SETS.KEY_COMPLEXID + " == ?",
                        new String[] {complexId}, null, null, null);
                if (cursorSet.moveToFirst())
                {
                    int countDeletedSet = db.delete(TABLE_SETS.TABLE_NAME, TABLE_SETS.KEY_COMPLEXID + " = " + complexId, null);
                    deletedData += "sets: " + countDeletedSet + "\n";
                }
            } while (cursorComplex.moveToNext());
            int countDeletedComplexes = db.delete(TABLE_COMPLEXES.TABLE_NAME, TABLE_COMPLEXES.KEY_TRAININGID + " = " + trainingId, null);
            deletedData += "complexes: " + countDeletedComplexes + "\n";
        }
        int countDeletedTrainings = db.delete(TABLE_TRAININGS.TABLE_NAME, TABLE_TRAININGS.KEY_DATE + " = " + date.getTime()/86400000, null);
        deletedData += countDeletedTrainings + "";
//        Toast.makeText(DayInfoActivity.this, deletedData, Toast.LENGTH_SHORT).show();
        cursor.close();
    }
    private void updateDayData()
    {
        cursor = db.query(TABLE_TRAININGS.TABLE_NAME, null, TABLE_TRAININGS.KEY_DATE + " == ?", new String[]{String.valueOf(date.getTime()/86400000)}, null, null, null);
        if (cursor.moveToFirst()) deleteDayData(); // delete daily data to update
        cursor.close();
        saveDayData();
        dataChanged = false;
    }
    private void initSListOfExercises()
    {
        cursor = db.query(TABLE_EXERCISES.TABLE_NAME, null, null, null, null, null, null);
        if (cursor.moveToFirst())
        {
            int nameIndex = cursor.getColumnIndex(TABLE_EXERCISES.KEY_NAME);
            do {
                listOfExercises.add(cursor.getString(nameIndex));

            } while (cursor.moveToNext());
        }
        cursor.close();
    }
    AdapterView.OnItemSelectedListener onItemSelectedListener()
    {
        return new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                countSets--;
                if (countSets < 0) dataChanged = true;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {}
        };
    }

}