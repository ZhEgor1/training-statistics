package com.example.trainingstatistics;

public class TABLE_MAJORMUSCLEGROUPSLIST {
    public static final String TABLE_NAME = "MajorMuscleGroupsList";
    public static final String KEY_MAJORMUSCLEGROUPID = "majorMuscleGroupId";
    public static final String KEY_TRAININGID = "trainingId";
}
